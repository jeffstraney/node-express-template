////// APPLICATION INDEX //////


//// Core Modules
const path = require('path'),
      querystring = require('querystring');


//// Custom Modules
const env    = require('app/core/env'),
      routes = require('app/route');


const {
  flash,
  init_session,
} = require('app/core/middleware');


const {
  application_port,
  session,
  session_store,
  site_root,
  site_title,
} = env;


//// Contributed Modules
const body_parser     = require('body-parser'),
      compression     = require('compression'),
      cookie_parser   = require('cookie-parser'),
      express         = require('express'),
      express_session = require('express-session'),
      helmet          = require('helmet'),
      moment          = require('moment'),
      serve_favicon   = require('serve-favicon');

const app = express();

app.use(body_parser.json());

app.use(body_parser.urlencoded({extended: true}));

// set up middleware
app.use(helmet());

// static files
app.use(express.static(path.join(__dirname, 'public')));

// TODO: improve readability of this line
session.store = new (require(session.store)(express_session))(session_store);

app.use(express_session(session));

app.use(flash());
app.use(init_session);

app.use(cookie_parser());
app.use(serve_favicon(path.join(__dirname, 'public', 'favicon.ico')));

app.use(compression());

// view configuration
app.set('view engine', 'pug');
app.set('views', path.join(__dirname, 'views'));

//// locals
app.locals.moment       = moment;
app.locals.querystring  = querystring;
app.locals.site_root    = site_root;
app.locals.site_title   = site_title;

// misc. locals
const {to_machine_name, to_url, canonical} = require('app/core/locals');

app.locals.to_machine_name = to_machine_name;
app.locals.to_url          = to_url;
app.locals.canonical       = canonical;

// use custom routes
app.use(routes);

// start application
app.listen(application_port, '127.0.0.1', function () {

  console.log(site_title + ' server started at port ' + application_port);
  
});

