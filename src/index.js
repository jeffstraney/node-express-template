import React from 'react';
import ReactDOM from 'react-dom';

window.onload = () => {

  const app = document.getElementById('app');

  const demo_text = "rendered by react for gorchoo";

  ReactDOM.render(
    <div>{demo_text}</div>,
    app
  );

  module.hot.accept();

};
