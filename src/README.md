# Client Side Source Code 

this is the directory that contains client side JS and CSS source (css source as less files).

I am a firm believer in separation of concern and never thought webpack bundling was a good fit for delivering static CSS for simple websites. However, if a site becomes very UI intensive, the benefit of bundling *some* CSS and images is apparent.

For that reason, any UI specific css for widgets should go along side any React components that require them. For all other css (styling of paragraphs, site images, navigation) goes directly in the less directory and is watched and compiled by less-watch-compiler (a very simple stylsheet compiler for less that *just works*).

## Remember

keep it declarative

keep it atomic

keep it functional
