////// USER CONTROLLER //////

//// Core Modules
const crypto    = require('crypto');


//// Contributed Modules
const argon2            = require('argon2'),
      base64url         = require('base64url'),
      entity_controller = require('entity-controller');



// model's
const User = require('app/core/model/user'),
      Auth = require('app/core/model/auth');


const controller = entity_controller({
  create: {
    on_pre_query: async (params) => {

      const {password} = params;

      const hash = await argon2.hash(password, {
        timeCost    : 2,
        memoryCost  : 2 ** 13,
        parallelism : 2,
        type        : argon2.argon2d
      });

      params.hash = hash;

      delete params[password];

    },        
    on_query: async (params) => {


      const user = User.build(params);

      const auth_token = Auth.build({
        username: user.username,
        hash: base64url(crypto.randomBytes(64))
      });
     
      user.save(); 
      auth.save();

      return user;

    },        
  },
  read : {
    on_query: async(params) => {

      const query = read_user_query;

      const user = await db.query(query, params)
        .catch((err) => { console.error(err); });

      console.log(user);

      return user;

    } 
  },
  update: {
    on_query: async(params) => {

      const query = "";

      return db.query(query, params);

    } 
  },
  destroy: {
    on_query: async(params) => {

      const query = ""; 

      return db.query(query, params);

    }
  }

});

module.exports = controller;
