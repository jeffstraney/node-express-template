const argon2 = require('argon2');

const entity_controller = require('');

const auth_model = require('app/model/auth');

const actions = {};

actions.create = {

  on_pre_query : async (params) => {

    const password = params.password;

    params.hash = await argon2.hash(password);

    delete params.password;

  },

  on_query : async (params) => {

    const auth = auth_model.create(params);

    auth.save();

    return auth;

  }

};

actions.authenticate = {

  on_query : async (params) => {

    auth.find

  }


};

const controller = entity_controller(actions);

return controller;
