const db = require('app/core/db');

const schema = {

  username: db.String,
  email: {
    type: db.String,
    validate: {
      isEmail: {
        msg: "Valid e-mail is required"
      }
    }
  },
  hash: db.String

};

const User = db.define('user', schema);

module.exports = User;
