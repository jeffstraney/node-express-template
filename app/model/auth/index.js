const db = require('app/core/db');

const user_model = require('app/model/user');


const schema = {
  username: db.String,
  hash:{
    type: db.String,
    primaryKey: true,
  }
};


const Auth = db.define(schema);


// each user get's one authentication
Auth.belongsTo(User, {foreignKey: 'username'});


module.exports = Auth;
