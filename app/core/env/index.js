const {NODE_ENV} = process.env;

const path = require('path');

const configs = require(path.join(__dirname, NODE_ENV)) || {};

if (!configs)
  throw new Error([
      'You must include a configuration file under app/core/env.',
      'Please see app/core/env/README.md or app/core/env/example.js to learn',
      'more about site configuration.'
      ].join(' '));

module.exports = configs;
