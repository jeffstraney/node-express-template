# IMPORTANT
if you are installing or maintaining this application, files have been excluded from this directory using version control (Git) for security reasons. For the application to work properly, you have to define your settings here in a new file named after your NODE_ENV environment variable (e.g. "development", "production"). Use the "example.js" file to see what options are available
