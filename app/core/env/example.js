///// EXAMPLE CONFIGURATION /////
"use strict";

const crypto = require('crypto');

// when npm start is run, the application will use the NODE_ENV
// shell variable to load the correct file. For instance, 
// if NODE_ENV == "development", then development.js will be loaded
// from this directory. The default environment is 'development',
// so copying this file into a file called 'development.js' is the
// the place to start.


// application level configs
const config = {
  application_port: 3000,
  site_title : "DEV - To Make 1000",
  site_root  : "http://localhost:3000",
};


// settings passed into session store. if this is a redis session
// store, you might put the password here, along with the port
config.session_store = {}


// replace with the session settings for your connect-session
// for 'store' property use the string name of the connect module
// used (e.g. connect-redis, or session-file-store)
config.session = {
  resave : false,
  saveUninitialized: false,
  secret: crypto.randomBytes().toStrin('hex'),
  cookie: {
    domain  : "localhost",
    httpOnly: true,
    // ten minute cookie life
    maxAge  : 60 * 1000 * 10 
  },
  // used dynamically to 'require' the proper session store constructor. 
  // from top level index.js, this property is reset with the initialized
  // session store
  store: "connect-redis"
}


config.db = {
  driver   : "mysql",
  database : "my_database",
  user     : "admin",
  password : ""
};


// mail for nodemailer
// docs: https://bit.ly/2z48woe
config.mail = {
  host: "somemailhost",
  auth: {
    user: "example@somemailhost.com",
    password: "1234",
  },
  port: 587,
};


module.exports = config;


