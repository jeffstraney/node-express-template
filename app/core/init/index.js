////// INIT //////
'use strict';


// sets various handlers for application
function configure (params) {

  params = params || {};

  process.on('exit', function () {

    process.stdin.resume();

    console.log('node server shutting down...');

  });

  process.on('SIGINT', function (err) {

    console.log("\nstopping node server\n");

    process.exit(2);

  });

  // TODO: set any other global process handlers

}

module.exports = configure;

