const fixtures = {};

fixtures.test_routes = [
  {
    url: '/view-page',
    allowed_params: 'view_page'
  },
  {
    url: '/edit-page',
    allowed_params: ['administrate_site', 'edit_page']
  },
  {
    url: '/delete-page',
    allowed_params: (req, res) => { req.session.current_user.name = 'test-2'}
  },
  {
    url: '/update-page',
    allowed_params: [
      'update_page',
      (req, res, current_user) => { return current_user.name == 'test-2'},
      'administrate_site',
    ]
  }
];

fixtures.test_users = [
  {
    name: 'test-1',
    permissions: {
      view_page: true,
    } ,
    cases: {
      '/view-page': true,
      '/edit-page': false,
      '/update-page': false,
      '/delete-page': false,
    }
  },
  {
    name: 'test-2',
    permissions: {
      administrate_site: true,
    } ,
    cases: {
      '/view-page': false,
      '/edit-page': true,
      '/update-page': true,
      '/delete-page': false,
    }
  },
  {
    name: 'test-3',
    permissions: {
      view_page: true,
      edit_page: true,
    }, 
    cases: {
      '/view-page': true,
      '/edit-page': true,
      '/update-page': false,
      '/delete-page': false,
    }
  },
  {
    name: 'test-4',
    permissions: {
      administrate_site: true
    },
    cases: {
      '/view-page': false,
      '/edit-page': true,
      '/update-page': true,
      '/delete-page': false,
    }
  }
];

module.exports = fixtures;

