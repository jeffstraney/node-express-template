const axios = require('axios');

const client = axios.create({
  baseURL: 'http://localhost:3000'
});

const {spawn} = require('child_process');

var server;

const start_server = async () => {

  server = spawn('export NODE_PATH=. && node test/server', [], {
    stdio: 'inherit',
    shell: true
  });

  server.on('error', stop_server);

};

let request_timeout = setTimeout(async () => {

  send_requests()
  .then(stop_server)
  .catch(stop_server);

}, 2500);

const stop_server = async (e) => {

  // stop request timeout
  clearTimeout(request_timeout);

  if (typeof e === 'object' && e.constructor === Error)
    console.error(e);

  server.kill('SIGINT');

};

const fixtures = require('test/fixtures');

const {test_users} = fixtures;

const log_response = (response) => {

  if (response.constructor === Error)
    console.error(response.message);

  else
    console.log(response.data);

};

const send_requests = async () => {

  test_users.forEach(async (user, index) => {

    // send out request to get user into session
    const auth_response = await client.get('/auth');

    const response_headers = auth_response.headers;

    const sess = response_headers['set-cookie'][0];

    const request_headers = {cookie: sess};

    for (let url in user.cases) {

      client.get(url, {headers: request_headers})
      .then(log_response)
      .catch(log_response);

    }

  });

};

start_server()
.catch(stop_server);

