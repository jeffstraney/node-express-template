A permission based authentication middleware for express. Works out of the box for common cases, but is configurable for complex cases

```js

// app = express() etc. etc.

const allowed = require('allowed')();

app.get('/payroll/edit', [
  allowed('view_payroll'),
  (req, res) => {

    // render page if user has 'view_payroll' permission
    res.render('payroll.pug', {});

  }
]);

```

The library requires that a connect session is being used (such that your current_user object is under the req.session object)

by default, the permissions are looked for under req.session.current_user.permissions[your_permission_here], as long as this value is asserted, the check will pass.

## I have multiple permissions to check

You can pass an array of strings into the middleware like so; using the previous example:

```js

const allowed = require('allowed')();

// now as long as the user has either 'administrate_payroll' OR 'view_payroll'
// (checked in that order) they may proceed
app.get('/payroll/edit', [
  allowed(['administrate_payroll', 'view_payroll']),
  (req, res) => {

    // render page if user has 'view_payroll' permission
    res.render('payroll.pug', {});

  }
]);
```

configuration can be passed into the middleware 

```js

// this application wide configuration only needs to be passed in once
const allowed = require('allowed')({
  // object key of user object in req.session
  current_user_key: 'user'
  // object key of permissions to check (you may use roles)
  permissions_key: 'roles'
});

app.get('/payroll/edit', [
  allowed(['administrate_payroll', 'view_payroll']),
  // etc...
]);

// the configuration can still be overwritten per route!
app.get('/page/delete', [
  allowed(['delete_page'], {permissions_key: 'permissions'})
])

```

## I need to check if the user is signed in

a courtesy middleware is provided out of the box, under as allowed.if_signed_in

```

const allowed = require('allowed')({redirect: 'https://mysite.com/sign-in'});

app.get('/account', [
  // simply checks for existing value under req.session[current_user_key]
  allowed.if_signed_in
  // ... render page etc.
]);

```

