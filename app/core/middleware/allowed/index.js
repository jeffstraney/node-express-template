////// ALLOWED /////
'use strict';


//// Core Modules
const url = require('url');


let allowed_configs;


function configure (top_configs) {

  top_configs = top_configs || {};

  // first call to library allows top level configuration
  allowed_configs = allowed_configs || top_configs;

  function allowed (permission, override_configs) {

    override_configs = override_configs || {};

    Object.assign(allowed_configs, override_configs);

    const logic = override_configs.logic || "or";

    // on denial, return json by default
    const on_deny = override_configs.on_deny ||
      function (req, res) { res.status(401).json({success: false}); };

    // on success, call next by default 
    const on_success = override_configs.on_success || 
      function (req, res, next) { next(); };

    // by default, use 'current_user' as the session key to check permission against
    const session_user_key = override_configs.session_user_key ||
      "current_user"; 

    // by default, use 'permissions' as the key under user object. could do
    // 'role' based authentication by changing key to 'roles'. As long as
    // the lookup value is asserted, then the user passes
    const permissions_key = override_configs.permissions_key ||
      "permissions"; 

    if (typeof permission === "function") {

      return async function (req, res, next) {

        // simply call
        const has_permission = permission(req, res);

        has_permission? on_success(req, res, next): on_deny(req, res, next);

        return;

      }

    }

    else if (permission.constructor === Array) {

      // return middleware that loops over the conditions provided
      return async function (req, res, next) {

        const session = req.session || {};

        // use session_user_key to look up user in session
        const current_user = session[session_user_key] || {};

        // use permission key to look up permission in user
        const permissions  = current_user[permissions_key]  || {};

        // start with false if using 'or' (default). start on true if 'and'
        var has_permission = logic == "and" ? true: false;

        // go through all permissions
        for (var i = 0; i < permission.length; i++) {

          const step = permission[i];

          var pass;

          if (typeof step == "string") {

            pass = permissions[step] || false;

          }
          else if (typeof step == "function") {

            // current_user passed as a courtesy
            pass = await step(req, res, current_user) || false;

          }
          else {

            pass = false;

          }

          if (logic == "and")
            has_permission = has_permission && pass;

          else
            has_permission = has_permission || pass;

        }

        if (has_permission == true)
          on_success(req, res, next);

        else 
          on_deny(req, res, next);

      }

    }

    else if (typeof permission === "string") {

      return async function (req, res, next) {

        const session = req.session || {};

        const current_user = session[session_user_key] || {};
        const permissions  = current_user.permissions  || {}; 

        const has_permission = permissions[permission];

        if (has_permission == true)
          on_success(req, res, next);

        else
          on_deny(req, res, next);

      }

    }

    else {

      throw new Error([
        'allowed middlewear expects a function, array, or string as an',
        'argument'].join(' '));

    }

  }

  // some top level permission checking middleware is defined here, using the
  // allowed module.
  allowed.if_signed_in = allowed(function (req, res, next) {

    // Anon user does not have id
    return typeof req.session.current_user === 'object';

  },
  { 
    on_deny : (req, res) => {

      const redirect = allowed_configs.redirect || top_configs.redirect || '/';

      res.redirect(url.resolve(redirect)); 

    }
  });

  // check body of request to match a session key
  allowed.if_req_matches_session = (reqkey, sesskey) => {

    return allowed(async (req, res, next) => {

      const body    = req.body || {},
            session = req.session || {};

      // note that this assumes that by performing this check, the
      // value of this is highly important (such as a csrf_token)
      // drops request
      return body[reqkey] && 
             session[sesskey] &&
             body[reqkey] === session[sesskey];

    });

  };

  return allowed;

}

module.exports = configure;
