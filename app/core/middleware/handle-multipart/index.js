////// HANDLE MULTIPART //////
"use strict";


//// Core Modules
const path = require('path');

const multer = require('multer');


function filter_file(whitelist) {

  if (!whitelist)
    throw new Error('you should really be specifying a whitelist for uploaded files');

  return (req, file, cb) => {

    for (var i in whitelist) {

      const allowed_mime = whitelist[i];

      if (file.mimetype == allowed_mime) {

        cb(null, true);

        return;

      } 

    }

    cb(null, false);

  }

}

// each endpoint where files are accepted could specify
// different filetype and size restrictions and could
// definitely require that they are stored in different
// places
function configure (configs) {

  configs = configs || {};

  const {whitelist, entity, limits} = configs;

  const storage = multer.diskStorage({
    destination: path.join('/tmp/tmo_uploads', entity),
    filename: (req, file, cb)=> {

      cb(null, file.fieldname + '-' + Date.now());

    }
  });

  const handle_multipart = multer({
    limits: limits,
    fileFilter: filter_file(whitelist),
    storage: storage
  }); 

  // allow chaining with 'single', 'array' as specified
  // by multer documentation
  return handle_multipart

}

module.exports = configure;
