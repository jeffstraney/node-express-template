////// MIDDLEWARE //////
"use strict";

// clone to perform deep copies
const clone = require('clone');

const configs = {};

function configure (configs) {

  // name of 404 template
  const {render_page_404} = configs;

}


function render_page (page, page_configs) {

  const config_params = page_configs || {};
  
  return async function (req, res) {

    var params = {};

    console.log(req.session);

    for (var param_key in config_params ) {

      const param = config_params[param_key];

      if(typeof param === "function") {

        // allow computed values based on request. allow res to be modified
        params[param_key] = await param(req, res);

      }
      else {

        // allow literal values
        params[param_key] = param;

      }

    }

    // maybe allow this to be changed by configs
    if (req.query && Object.keys(req.query).length) {
      params = Object.assign(params, req.query);
    }

    if (req.params && Object.keys(req.params).length) {
      params = Object.assign(params, req.params);
    }

    // console.log(params);

    res.render(page, params);

    // remove any flash messages 
    // TODO: allow flash msg key to be configurable
    delete req.session.flash_msg;

  }

}

module.exports = render_page;
