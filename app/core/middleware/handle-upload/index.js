////// HANDLE UPLOAD //////
"use strict";


//// Core Modules
const path   = require('path'),
      crypto = require('crypto'),
      fs     = require('fs-promise');


function configure (configs) {

  configs = configs || {};

  // assumed to be joined by path module before call
  const {dir, fields} = configs;

  if (!fields) {
    throw new Error([
      'you must tell handle_upload middleware which field key',
      'the files are stored under. pass a string, or array'
    ].join(' '));
  }

  if (fields.constructor != String && fields.constructor !== Array) {
    throw new Error([
      '"fields" key expects a string or array for handle_upload middleware'
    ].join(' '));
  }

  // random hash if no filename
  const get_filename = configs.filename || ((req, file) => file.filename );

  // assumes that multer was used to put files in 'file'
  // property of request. At this point, files should be validated
  return async function (req, res, next) {

    const {file} = req;

    const filename =
      typeof get_filename === "function" ?
      get_filename(req, file) :
      get_filename;

    console.log('DEBUG UPLOADINGGG', file);

    // do for each file 
    if (file.constructor !== Array) {

      console.log('DEBUG FILE UPLAOD', file.path, path.join(dir, file.filename));

      fs.rename(file.path, path.join(dir, file.filename))
        .catch((e) => { console.error(e); });

      next();

      return;

    }

    // cross that bridge when we get there...
    for (var i in file) {

      console.log("TODO: implement multi file upload", file);

    }

    next();

  }

}

module.exports = configure;
