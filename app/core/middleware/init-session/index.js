///// INIT SESSION /////
"use strict";

const crypto = require('crypto');

const base64url = require('base64url');

function init_session(req, res, next) {

  const session = req.session || {};

  // empty object allows more graceful checking of properties (like permissions)
  // consider this the 'anonymous' user
  const current_user = session.current_user || {};

  // explicit null value for unambiguous checks
  const flash_msg = session.flash_msg || null;

  // set variables to be available fo views
  res.locals.current_user = current_user;
  res.locals.flash_msg    = flash_msg;

  // store values in the session
  session.current_user = current_user;
  session.flash_msg = flash_msg;

  // set a CSRF token which is good per session
  session.csrf_token = session.csrf_token || base64url(crypto.randomBytes(64));

  res.locals.csrf_token = session.csrf_token;

  return next();

}

module.exports = init_session;
