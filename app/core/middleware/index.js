////// MIDDLEWARE INDEX //////
"use strict";

//// Applicaiton Middleware
const allowed          = require('app/core/middleware/allowed'),
      flash            = require('app/core/middleware/flash'),
      handle_multipart = require('app/core/middleware/handle-multipart'),
      handle_upload    = require('app/core/middleware/handle-upload'),
      init_session     = require('app/core/middleware/init-session'),
      render_page      = require('app/core/middleware/render-page');


module.exports = {
  allowed,
  flash,
  handle_multipart,
  handle_upload,
  init_session,
  render_page,
};
