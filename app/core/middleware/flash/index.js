////// FLASH //////
"use strict";


function flash() {

  // allows 'flash' method to be called on req object
  // which stores a flash object in the session
  return async function (req, res, next) {

    if (!req.session)
      throw new Error('flash middleware requires a connect session.');

    req.app.locals.flash_msg = req.session.flash_msg || null;

    delete req.session.flash_msg;

    req.flash = req.flash || function (text, type) {

      type = type || 'info';

      if (req.session) {

        // added to session. init_session adds this to locals for templates
        req.session.flash_msg = {text, type};

      }

      else {

        throw new Error('flash middleware requires an implementation of session to be used');

      }

    }

    return next();

  };

}


module.exports = flash;

