function to_url (str) {

  return str.trim().replace(/\W/g, '-');

}

module.exports = to_url 
