// canonical site information, descriptions, and more
const canonical = {};
canonical.machine_name_title = 'Underscore delimited identifier that is usable by scripts.';
canonical.label_title        = 'Human readable lable for site visitors';

module.exports = canonical;

