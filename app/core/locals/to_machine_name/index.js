function machine_name (label) {

  return label.trim().replace(/\W/g, '_');

}

module.exports = machine_name;
