const locals = {};

const {site_root, site_title} = require('app/core/env');

locals.site_root  = site_root;
locals.site_title = site_title;

locals.canonical       = require('app/core/locals/canonical');
locals.to_machine_name = require('app/core/locals/to_machine_name');
locals.to_url          = require('app/core/locals/to_url');

module.exports = locals;
