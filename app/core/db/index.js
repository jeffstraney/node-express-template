const db_config = require('app/core/env').db || {};

const Sequelize = require('sequelize');

const {
  database,
  host,
  user,
  driver
} = db_config;

const password = db_config.password || "",
      min_pool = db_config.min_pool || 2,
      max_pool = db_config.max_pool || 20;

const db = new Sequelize(database, user, password, {
  host   : host,
  dialect: driver ,
  operatorsAliases: Sequelize.Op,
  pool: {
    min: min_pool,
    max: max_pool,
    acquire: 30000,
    idle: 10000
  }
});
