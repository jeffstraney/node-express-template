const path = require('path');

const fs   = require('fs-promise');

const {QueryFile} = require('pg-promise');

const pg = require('pg-promise')();

const monitor = require('pg-monitor');

function error (err, e) {

  err.is_error = true;

  e.is_error = true;
 
  monitor.error(err, e); 
 
}

const monitor_options = {
  error
}; 

monitor.attach(monitor_options);

const configs = require('app/core/env').pgsql || {};

const db = pg(configs);

async function get_sql (dir, file_name) {

  const full_path = path.join(dir, '/queries/', file_name);

  return new QueryFile(full_path, {minify: true});

}

module.exports = {db, get_sql};
