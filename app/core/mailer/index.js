////// MAILER //////
'use strict';

//// Contributed Modules ////
const nodemailer = require('nodemailer');
const pug        = require('pug');

// get settings for the environment we are running in
const NODE_ENV   = process.env.NODE_ENV;
const settings   = require('path-labs/core/env_settings')(NODE_ENV);

// template functions keyed by path
const templates = {};

// global configs for mailing and templating. may be
// overwritten by configure function
function template (path, params) {

  // only compile once
  templates[path] = templates[path] || pug.compileFile(path);

  // now we will always have a function to render
  const fn = templates[path];

  // return the html rendered using params
  var html = fn(params);

  return html

}

//// Configuration Entry Point ////
// if necessary, allow configs to be put in here
function configure (configs) {

  const mailer_configs = settings.mailer || {};

  configs = configs || {};

  for (var i in configs) {

    mailer_configs[i] = configs[i];

  }

  const transporter = nodemailer.createTransport(settings.mailer);
  
  // return interace to nodemailer in addition to a templating function
  return {
    sendMail: function (options) {

      options.from = options.from || mailer_configs.auth.user;

      return transporter.sendMail(options);

    },

    template: template

  };

}

module.exports = configure;
