////// ROUTE INDEX //////
'use strict';

const 
      express = require('express'),
      router = express.Router();

const 
      auth = require('app/route/auth'),
      page = require('app/route/page'),
      user = require('app/route/user');

router.use(auth);
router.use(user);
router.use(page);

module.exports = router;
