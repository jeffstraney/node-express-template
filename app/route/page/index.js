////// PAGE INDEX /////
const express = require('express');


//// Contributed Modules
const router = express.Router();


//// Custom Modules
const {render_page} = require('app/core/middleware');


//// Page Routes
router.get('/', render_page('page/home', {
  page_title: "How to make Your First $1000 at Anything",
}));

router.get('/about', render_page('page/about', {
  page_title: "About",
}));

router.get('/help', render_page('page/help', {
  page_title: "Help",
}));

router.get('/terms', render_page('page/terms', {
  page_title: "Terms",
}));

router.get('/privacy', [render_page('page/privacy', {
  page_title: "Privacy",
})]);

// catch all routes
router.get('/*', [(req, res, next) => {

  if (!res.headersSent)
    next()

}, render_page('page/404', {
  page_title: "404",
})]);

module.exports = router;
