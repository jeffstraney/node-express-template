////// USER INDEX //////
"use strict";


//// Contributed Modules 
const express = require('express'),
      router  = express.Router();


//// Custom Modules 
const {
  allowed,
  render_page,
} = require('app/core/middleware');

const allow = allowed();

const controller      = require('app/controller/user'),
      {handle_action} = controller;


//// Pages 

// helper function to use current user in forms and templates
// this is used because not all information about a user is stored
// in the current_user session object. 

const use_current_user = async (req, res) => {

  const id = req.session.current_user.id || 0;

  // read the current user, using session.current_user id
  return await controller.read({id: id});

};

// signed in user profile
router.get('/account', [
  allow.if_signed_in,
  render_page('user/view', {
    page_title: "View Account",
    user : use_current_user 
  })
]);

// page to edit own user information. must be signed in
router.get('/account/edit', [
  allow.if_signed_in,
  render_page('user/edit', {
    page_title: "Edit Account",
    user : use_current_user 
  })
]);

router.get('/admin/user/new', [ 
  allow(['administrate_user', 'create_user']),
  render_page('user/new', {
    page_title: "Create User"  
  })
]);

// admin page to view and edit users
router.get('/admin/users', [ 
  allow('administrate_user'),
  render_page('user/admin', {
    page_title: "Administrate Users"  
  })
]);

const if_is_user = async (req, res, current_user) => {

  return req.params.id == current_user.id;

};

const use_user_from_param = async (req, res) => {

  const user = await controller.read(req.params);

  return  user;

};

// route to view any user information. must be admin
router.get('/users/:id/profile', [
  allow.if_signed_in,
  allow([if_is_user, 'administrate_user']),
  render_page('user/view', {
    page_title: 'View Profile',
    user: use_user_from_param  
  })
]);

// route to edit any arbitrary user. restricted
router.get('/users/:id/edit', [
  allow.if_signed_in,
  allow([if_is_user, 'administrate_user', 'update_user']),
  render_page('user/edit', {
    page_title: 'Edit Profile',
    user: use_user_from_param  
  })
]);

///// API Endpoints
router.post('/user', handle_action('create'));
router.get('/user/:id', handle_action('read'));
router.put('/user/:id', handle_action('update'));
router.delete('/user/:id', handle_action('destroy'));

// export export export export export export export export export export 
module.exports = router;

