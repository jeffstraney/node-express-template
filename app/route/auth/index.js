////// AUTH INDEX //////

const router = require('express').Router();


const render_page = require('app/core/middleware/render-page'),
      allowed     = require('app/core/middleware/allowed')();


const {handle_action} = require('app/controller/auth');


router.get('/sign-up', render_page('auth/sign-up', {
  page_title: 'Sign Up' 
}));


router.get('/sign-in', render_page('auth/sign-in', {
  page_title: 'Sign In' 
}));

router.post('/sign-up', [
  allowed.if_req_matches_session('csrf_token', 'csrf_token'),
  handle_action('create')
]);

router.post('/sign-in', [
  allowed.if_req_matches_session('csrf_token', 'csrf_token'),
  handle_action('sign_in')
]);

module.exports = router;
