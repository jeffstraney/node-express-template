////// WEBPACK CONFIG


const {NODE_ENV} = process.env;

const path = require('path');


const webpack                 = require('webpack'),
      UglifyJsPlugin          = require('uglifyjs-webpack-plugin');
     

const config = {};

config.mode = NODE_ENV || 'development';

const is_dev = NODE_ENV == "development";

config.entry = {
  js: "./src/index.js"
};

config.optimization = {
  minimizer : [
    new UglifyJsPlugin({
      cache: true,
      parallel: true,
      sourceMap: false,
    }),
  ]
}

config.module = {
  rules: [
    {
      test: /\.(js|jsx)$/,
      exclude: '/node_modules/',
      use : ['babel-loader']
    },
  ]
};

config.resolve = {
  extensions : ['*', '.js', '.jsx']
};

config.output = {
  path: path.join(__dirname, 'public/js/dist'),
  publicPath: './public/',
  filename: 'bundle.js'
};

config.plugins = [
  new webpack.HotModuleReplacementPlugin(),
];

config.devServer = {
  contentBase: './public/dist'
};

module.exports = config;
